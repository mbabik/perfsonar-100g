import pandas as pd

d1 = pd.read_csv('s1.ddat', sep=' ',header=None, names=['time', 'bytes', 'speed', 'retrans', 'cwnd'])
d2 = pd.read_csv('s2.ddat', sep=' ',header=None, names=['time', 'bytes', 'speed', 'retrans', 'cwnd'])
d3 = pd.read_csv('s3.ddat', sep=' ',header=None, names=['time', 'bytes', 'speed', 'retrans', 'cwnd'])

fd = d1.reset_index().merge(d2.reset_index(), left_index=True, right_index=True, how='left')
fd = fd.reset_index().merge(d3.reset_index(), left_index=True, right_index=True, how='left')

print fd

fd.to_csv('s_merged.ddat', sep=' ', columns=['time', 'speed', 'speed_x', 'speed_y'], index=False, header=False)
