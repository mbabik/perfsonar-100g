#
# sample Gnuplot command file for iperf3 results
#set term x11

set term png 

#set term postscript landscape color
set key width -12

#iperf3 data fields
#start bytes bits_per_second retransmits snd_cwnd

set output "iperf3_lan_mtu.png"
#set output "iperf3.eps"

#set nokey

set grid xtics
set grid ytics
set grid linewidth 1
set title "TCP performance: 100G (LAN, iperf3, single stream)"
set xlabel "time (seconds)"
set ylabel "Bandwidth (Gbits/second)"
set xrange [0:60] 
set yrange [0:45] 
set ytics nomirror
set y2tics
set y2range [0:45] 
# dont plot when retransmits = 0
set datafile missing '0'
set pointsize 1.0
set key right bottom

firstcol=2
#cumulated(i)=((i>firstcol)?column(i)+cumulated(i-1):(i==firstcol)?column(i):1/0)
#plot "s_merged.ddat" using 1:(cumulated(4)) title 'stream-1' with linespoints lw 3 pt 5,\
#     "" using 1:(cumulated(3)) title 'stream-2' with linespoints lw 3 pt 7,\
#     "" using 1:(cumulated(2)) title 'stream-3' with linespoints lw 3 pt 9

#plot "s1.ddat" using 1:3 title 'stream-1' with linespoints lw 3 pt 5, \
#     "s2.ddat" using 1:3 title 'stream-2' with linespoints lw 3 pt 7, \
#     "s3.ddat" using 1:3 title 'stream-3' with linespoints lw 3 pt 9, \
#
#      "s3.ddat" using 1:3 title 'stream-3' with points pt 7 axes x1y2
#

plot "lan_1_1500mtu.dat" using 1:3 title 'MTU 1500' with linespoints lw 3 pt 5, \
     "lan_1_9000mtu.dat" using 1:3 title 'MTU 9000' with linespoints lw 3 pt 7

