# perfsonar-100g-testing

This is a collection of results and scripts used to process them in different 100g testing studies. This work is based on ESNet perfsonar 100G testing and extends some of the scripts provided by perfSONAR team.

Raw data is stored in either .txt files (for iperf2 - has no json output) or in json (for iperf3) - sample test commands are in test_commands.txt

iperf2_parser.py can be used to parse iperf2 text results into csv/dat format

iperf3_to_gnuplot script can be used to parse iperf3 test results into csv/dat format

iperf3_to_gnuplot2 script can be used to parse pscheduler iperf3 json results into csv/dat format

merge_dat.py can be used to read multiple dat files and compute cumulative numbers (for stack based graphs) - only needed if trying to join multiple dat files (single dat files are using a cumulative function in gp)

collection of .gp files shows different ways of visualising dat files


