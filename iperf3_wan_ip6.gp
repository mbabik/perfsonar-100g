#
# sample Gnuplot command file for iperf3 results
#set term x11

set term png 

#set term postscript landscape color
set key width -12

#iperf3 data fields
#start bytes bits_per_second retransmits snd_cwnd

set output "iperf3_wan_ip6.png"
#set output "iperf3.eps"

#set nokey

set grid xtics
set grid ytics
set grid linewidth 1
set title "TCP performance: 100G (WAN, iperf3, single stream, 16ms RTT)"
set xlabel "time (seconds)"
set ylabel "Bandwidth (Gbits/second)"
set xrange [0:60] 
set yrange [0:45] 
set ytics nomirror
set y2tics
set y2range [0:45] 
# dont plot when retransmits = 0
set datafile missing '0'
set pointsize 1.0
set key right bottom

firstcol=2
#cumulated(i)=((i>firstcol)?column(i)+cumulated(i-1):(i==firstcol)?column(i):1/0)
#plot "s_merged.ddat" using 1:(cumulated(4)) title 'stream-1' with linespoints lw 3 pt 5,\
#     "" using 1:(cumulated(3)) title 'stream-2' with linespoints lw 3 pt 7,\
#     "" using 1:(cumulated(2)) title 'stream-3' with linespoints lw 3 pt 9

#plot "s1.ddat" using 1:3 title 'stream-1' with linespoints lw 3 pt 5, \
#     "s2.ddat" using 1:3 title 'stream-2' with linespoints lw 3 pt 7, \
#     "s3.ddat" using 1:3 title 'stream-3' with linespoints lw 3 pt 9, \
#
#      "s3.ddat" using 1:3 title 'stream-3' with points pt 7 axes x1y2
#

plot "wan_1_9000mtu_cpuperf_htoff_0copy_fq.dat" using 1:3 title 'IPv4' with linespoints lw 3 pt 5, \
     "wan_1_9000mtu_cpuperf_htoff_0copy_fq_ip6.dat" using 1:3 title 'IPv6' with linespoints lw 3 pt 7

