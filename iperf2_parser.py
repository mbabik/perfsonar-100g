#!/bin/env python

from collections import defaultdict
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse iperf2 output.')
    parser.add_argument('in_file', help='input file (iperf2 output as txt)')
    parser.add_argument('out_file', help='csv output file')
    args = parser.parse_args()
    results = defaultdict(list)
    with open(args.in_file, 'r') as f:
        lines = f.readlines()

    for line in lines:
        if '[' in line:
            sl = line.split(' ')
            sl = [ i for i in sl if i ]
            try:
                index_gbps = sl.index('Gbits/sec\n')
            except ValueError:
                continue
            if '[SUM]' not in sl[0]:
                results[float(sl[2].split('-')[0])].append(sl[index_gbps-1])
    with open(args.out_file, 'w') as f_w:
        for k in sorted(results):
            f_w.write('{} {}\n'.format(k, ' '.join(results[k])))
        
