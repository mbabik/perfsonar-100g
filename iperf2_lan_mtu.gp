#
# sample Gnuplot command file for iperf3 results
#set term x11

set term png  size 1200, 400
firstcol=2
cumulated(i)=((i>firstcol)?column(i)+cumulated(i-1):(i==firstcol)?column(i):1/0)
set multiplot layout 1,2 title "TCP performance: 100G (LAN, iperf2, 8 streams)" 

#set term postscript landscape color
set key width -12

#iperf3 data fields
#start bytes bits_per_second retransmits snd_cwnd

#set output "iperf2_lan_mtu.png"
#set output "iperf3.eps"

#set nokey

set grid xtics
set grid ytics
set grid linewidth 1
set title "MTU 1500"
set xlabel "time (seconds)"
set ylabel "Cumulative Bandwidth (Gbps)"
set xrange [0:60] 
set yrange [0:100] 
set ytics nomirror
set y2tics
set y2range [0:100] 
# dont plot when retransmits = 0
set datafile missing '0'
set pointsize 1.0
set key off

plot "lan_8_iperf2_1500mtu_cpueco.dat" using 1:(cumulated(2)) title 's1' with linespoints lw 3 pt 1,\
     "" using 1:(cumulated(3)) title 's2' with linespoints lw 3 pt 2,\
     "" using 1:(cumulated(4)) title 's3' with linespoints lw 3 pt 5,\
     "" using 1:(cumulated(5)) title 's4' with linespoints lw 3 pt 7,\
     "" using 1:(cumulated(6)) title 's5' with linespoints lw 3 pt 9,\
     "" using 1:(cumulated(7)) title 's6' with linespoints lw 3 pt 10,\
     "" using 1:(cumulated(8)) title 's7' with linespoints lw 3 pt 12,\
     "" using 1:(cumulated(9)) title 's8' with linespoints lw 3 pt 13

set grid linewidth 1
set title "MTU 9000"
set xlabel "time (seconds)"
#unset ylabel
set xrange [0:60]
set yrange [0:100]
set ytics nomirror
set y2tics
set y2range [0:100]
# dont plot when retransmits = 0
set datafile missing '0'
set pointsize 1.0
set key off

plot "lan_8_iperf2_9000mtu_cpueco.dat" using 1:(cumulated(2)) title 's1' with linespoints lw 3 pt 1,\
     "" using 1:(cumulated(3)) title 's2' with linespoints lw 3 pt 2,\
     "" using 1:(cumulated(4)) title 's3' with linespoints lw 3 pt 5,\
     "" using 1:(cumulated(5)) title 's4' with linespoints lw 3 pt 7,\
     "" using 1:(cumulated(6)) title 's5' with linespoints lw 3 pt 9,\
     "" using 1:(cumulated(7)) title 's6' with linespoints lw 3 pt 10,\
     "" using 1:(cumulated(8)) title 's7' with linespoints lw 3 pt 12,\
     "" using 1:(cumulated(9)) title 's8' with linespoints lw 3 pt 13
unset multiplot


#plot "s1.ddat" using 1:3 title 'stream-1' with linespoints lw 3 pt 5, \
#     "s2.ddat" using 1:3 title 'stream-2' with linespoints lw 3 pt 7, \
#     "s3.ddat" using 1:3 title 'stream-3' with linespoints lw 3 pt 9, \
#
#      "s3.ddat" using 1:3 title 'stream-3' with points pt 7 axes x1y2
#

#plot "lan_1_9000mtu_cpuperf.dat" using 1:3 title 'cpupower set to performance' with linespoints lw 3 pt 5, \
#     "lan_1_9000mtu_cpuperf_htoff.dat" using 1:3 title 'hyperthreading off' with linespoints lw 3 pt 7, \
#     "lan_1_9000mtu_cpuperf_htoff_numa.dat" using 1:3 title 'cpu affinity tuning' with linespoints lw 3 pt 9
